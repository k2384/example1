import glitter
import time
soc = glitter.connectSocketApp()
s = glitter.connectSocketSite()
print("Welcome to glitter Swissknife: ")
username = input("Enter your username: ")
password = input('Enter your password: ')
print("loading...\n")
client_msg = """100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##""" % (username,password)
soc.sendall(client_msg.encode())
sMsg = soc.recv(4094)
sMsg = sMsg.decode()
while "doesn't exist" in sMsg or "Illegal user login" in sMsg or len(username)<5 or len(username)>20:
    print("no such username or password | process failed\n")
    username = input("Enter your username: ")
    password = input('Enter your password: ')
    print("loading...\n")
    client_msg = """100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##""" % (username,password)
    soc.sendall(client_msg.encode())
    sMsg = soc.recv(4094)
    sMsg = sMsg.decode()
time.sleep(2)
print("MAIN: ")
choice = 0
while(int(choice)!=11):
    print("BUG BOUNTY CHALLENGES:\n1. login challenge\n2. password challenge\n\nREGULAR WEAKNESSES:\n3. increase 10 likes to one of your glitts\n4. get user details from login\n5. check if user connected or not\n6. find emails of users by search\n7. post glit by a stranger\n8. post glit from the future\n9. change screen name to existing one\n10. post glit in a color that is not on the menu\n11. Exit\n")
    choice = input("Enter choice: ")
    while(int(choice)<1 or int(choice)>11):
        choice = input("choice out of range, Enter again: ")
    if choice=='1':
        print("usernames with their password:")
        glitter.find_user_credentials(2)
        print("\n")
    if choice=='2':
        user_name = input("Enter username: ")
        glitter.password_challenge(user_name)
        print("\n")
    if choice=='3':
        glitter.increase_likes(username, password)
        print("\n")
    if choice=='4':
        print("user details: ")
        glitter.weakn_user_details(username, password)
        print("\n")
    if choice=='5':
        user_to_check = input("Enter the name of the user you want to check if he is connected or not: ")
        glitter.weakn_user_status(username, password, user_to_check)
        print("\n")
    if choice=='6':
        glitter.weakn_email_by_search(username, password)
        print("\n")
    if choice=='7':
        glitter.post_by_stranger(username, password)
        print("\n")
    if choice=='8':
        glitter.post_glit_from_future(username, password)
        print("\n")
    if choice=='9':
        glitter.change_screen_name_existing(username, password)
        print("\n")
    if choice == '10':
        glitter.post_glit_color(username,password)
        print("\n")
    if choice=='11':
        print("GoodBye :( \n")
